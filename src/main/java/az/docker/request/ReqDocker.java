package az.docker.request;

import lombok.Data;

@Data
public class ReqDocker {

    private Long dockerId;
    private String name;
    private String surname;
    private String address;
}
