package az.docker.controller;

import az.docker.model.Customer;
import az.docker.request.ReqDocker;
import az.docker.service.DockerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class DockerController {
    private final DockerService  dockerService;

    @PostMapping("/addDocker")
    public void addDocker(@RequestBody ReqDocker reqDocker){
        dockerService.saveDockerValue(reqDocker);
    }

    @GetMapping("/get")
    public List<Customer> getUser(){
     return dockerService.findAllUser();
    }
}
