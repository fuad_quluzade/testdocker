package az.docker.service;

import az.docker.model.Customer;
import az.docker.repository.DockerRepository;
import az.docker.request.ReqDocker;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DockerService {

    private final DockerRepository dockerRepository;

    public void saveDockerValue(ReqDocker reqDocker) {
        Long id = reqDocker.getDockerId();
        String name = reqDocker.getName();
        String surname = reqDocker.getSurname();
        String address = reqDocker.getAddress();

        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setSurname(surname);
        customer.setAddress(address);
        dockerRepository.save(customer);
    }

    public List<Customer> findAllUser() {
        List<Customer> all = dockerRepository.findAll();
        return all;
    }
}
